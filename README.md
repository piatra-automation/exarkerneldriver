# ExarKernelDriver

<!-- @date 1 September 2022 -->

This driver is a fork of the basic ExarKernelDriver distributed with the ClearPath
motors linux install zip.

The original ExarKernelDriver was tested on versions of the kernel up to v3.16.

This version of the driver implements some changes required to get the driver
working on 5.15.
In this latest version of the kernel, at the time of writing part of the
raspberry pi OS (bullseye debian), the linux kernel headers have removed some
deprecated aliases and the driver will not compile on linux kernels 5.15+.

These small changes to the source will use the new functions present in the
headers - and should compile without issue using the identical installation shell
script.

## Update for RPI kernel 6.1.21-v8+

There seems to be an issue with the build directory being absent from `6.1.21-v8+` versions of the kernel. I had success with using the version `6.1.21-v7` by editing the `/boot/config.txt` file to have the line `arm_64bit=0`. See [link](https://forums.raspberrypi.com/viewtopic.php?t=350269)

## Usage

``` bash
cd exarkerneldriver
sudo su
./Install_DRV_SCRIPT.sh
exit
```

## Changelog

### Changed in v1.0.0

- Calls to deprecated `put_tty_driver()` function replaced with `tty_driver_kref_put()`.
- Allocating TTY via `tty_alloc_driver(XR_USB_SERIAL_TTY_MINORS, 0)` instead of `alloc_tty_driver(XR_USB_SERIAL_TTY_MINORS)`.
- Checking state via `test_bit(ASYNCB_INITIALIZED, &xr_usb_serial->port.flags)` replaced with `tty_port_initialized(&xr_usb_serial->port)`.

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
